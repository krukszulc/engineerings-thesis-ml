import time

import pandas as pd

from deep_learning import run
from ml_model.model import Model
from ml_model.shift import get_ideal_nonlinear_shift_model, get_shift_model, get_ideal_linear_shift_model


def main():
    # Tworzenie nowych danych dla modelu realnego
    print('hello')


train_data = pd.read_csv('data/examples_for_model/train_shift.csv')
test_data = pd.read_csv('data/examples_for_model/test_shift.csv')
run(train_data, test_data)


# ShiftData.create_test_for_shift_model()
# ShiftData.create_train_for_shift_model()
# Predykcje oraz zdefiniowanie modeli
# define_ideal_linear_models()
# define_ideal_nonlinear_models()
# test_data2 = pd.read_csv('data/examples_for_model/train_shift.csv')
# PlotData.plot_scatter_matrix(test_data2)
# define_real_model_gear(5.0)
# define_real_model_gear(4.0)
# prediction_all = define_real_model()
# algorytm = 'SVR'
# df = pd.DataFrame({
#     algorytm+'': prediction_all[0:]
# })
# df.to_csv('data/'+algorytm+'.csv')

def define_ideal_nonlinear_models():
    train_data_nonlineary = pd.read_csv('data/examples_for_model/train_nonlinear.csv')
    test_data_nonlineary = pd.read_csv('data/examples_for_model/test_nonlinear.csv')
    model_ideal_nonlineary = get_ideal_nonlinear_shift_model()
    model_ideal_nonlineary.train(train_data_nonlineary)
    test_x_nonlineary, test_y_nonlineary = model_ideal_nonlineary.prepare_data(test_data_nonlineary)
    predict_nonlineary = model_ideal_nonlineary.predict(test_x_nonlineary)
    Model.mae(test_y_nonlineary, predict_nonlineary)
    Model.percent_average_error(test_y_nonlineary, predict_nonlineary)


def define_ideal_linear_models():
    train_data_lineary = pd.read_csv('data/examples_for_model/train_linear.csv')
    test_data_lineary = pd.read_csv('data/examples_for_model/test_linear.csv')
    model_ideal_lineary = get_ideal_linear_shift_model()
    model_ideal_lineary.train(train_data_lineary)
    test_x_lineary, test_y_lineary = model_ideal_lineary.prepare_data_braking_charts(test_data_lineary)
    predict_lineary = model_ideal_lineary.predict(test_x_lineary)
    Model.mae(test_y_lineary, predict_lineary)
    Model.percent_average_error(test_y_lineary, predict_lineary)


def define_real_model_gear(gear):
    train_data2 = pd.read_csv('data/examples_for_model/train_shift.csv')
    test_data2 = pd.read_csv('data/examples_for_model/test_shift.csv')
    model = get_shift_model()
    train_data = train_data2.loc[train_data2['Gear'] == gear]
    test_data = test_data2.loc[test_data2['Gear'] == gear]
    # PlotData.plot_scatter_matrix(train_data)
    model.train(train_data)
    test_x, test_y = model.prepare_data(test_data)
    prediction = model.predict(test_x)
    Model.mae(test_y, prediction)
    Model.percent_average_error(test_y, prediction)
    return prediction
    # TODO klasa PlotData wyswietla zaleznosci itd..
    # PlotData.plot_scatter_matrix(train_data2)
    # PlotData.plot_depedencies(train_data2)


def define_real_model():
    train_data = pd.read_csv('data/examples_for_model/train_shift.csv')
    test_data = pd.read_csv('data/examples_for_model/test_shift.csv')
    model = get_shift_model()
    model.train(train_data)
    test_x, test_y = model.prepare_data(test_data)
    prediction = model.predict(test_x)
    Model.mae(test_y, prediction)
    # model.export_graph()
    Model.percent_average_error(test_y, prediction)
    return prediction
    # PlotData.plot_scatter_matrix(train_data)
    # PlotData.plot_depedencies(train_data)


start_time = time.time()
main()
print("--- %s seconds ---" % (time.time() - start_time))
