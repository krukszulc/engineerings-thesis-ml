from Tools.scripts import google
from firebase_admin import credentials, firestore
import firebase_admin


class Firebase:
    pathServiceAccKey = "common/firebase/serviceAccountKey.json"
    cred = credentials.Certificate(pathServiceAccKey)
    CARS = u'Registered Cars'
    TEMP_VIN = u'tempVin'
    LIVE_PARAMETERS = u'Live Parameters'

    def __init__(self):
        firebase_admin.initialize_app(self.cred, {
  'projectId': 'engineering-thesis-b689e',
})
        self.db = firestore.client()
        self.docReference = self.db.collection(self.CARS).document(self.TEMP_VIN).collection(self.LIVE_PARAMETERS)

    def saveData(self):
        self.docReference.add({
            u'speed': 45,
            u'rpm': 3255,
        })

    def getData(self):
        try:
            data = self.docReference.get()
            for doc in data:
                print(u'Document data: {}'.format(doc.to_dict()))
        except google.cloud.exceptions.NotFound:
            print(u'No such document!')
