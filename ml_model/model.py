from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.base import BaseEstimator, TransformerMixin
from constants import show_prediction
from sklearn.tree import export_graphviz
import time

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, atrr):
        self.attr = atrr

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X[self.attr].values


class Model:
    def __init__(self, pipeline, attr_x, attr_y):
        self.attributes_x = attr_x
        self.attributes_y = attr_y
        self.model = pipeline

    def train(self, data):
        start_time = time.time()
        train_x, train_y = self.prepare_data(data)
        self.__teach_model(train_x, train_y)
        print("Uczenie modelu trwało: % sekund" % (time.time() - start_time))

    def __teach_model(self, train_x, train_y):
        self.model.fit(train_x, train_y.ravel())


    def prepare_data(self, data):
        pipeline_x = Pipeline([
            ('convert', DataFrameSelector(self.attributes_x))])
        pipeline_y = Pipeline([
            ('convert', DataFrameSelector(self.attributes_y))])
        x = pipeline_x.fit_transform(data)
        y = pipeline_y.fit_transform(data)
        return x, y

    def export_graph(self):
        export_graphviz(self.model, out_file="obraz.dot")

    def predict(self, x):
        return self.model.predict(x)

    @staticmethod
    def mae(y, predictions):
        mae = mean_absolute_error(y, predictions)
        print("mae: " + str(mae))
        return mae

    @staticmethod
    def percent_average_error(y, predictions):
        percent_error = []
        for index in range(0, len(y)):
            percent_error.append(abs(((y[index] - predictions[index]) / (y[index])) * 100))
            if show_prediction:
                print("Predict: " + str(predictions[index]) + ", test value:" + str(
                    y[index]))

        percent_mae = sum(percent_error) / len(percent_error)
        print("Percent mae: " + str(percent_mae))
        return percent_mae

    @staticmethod
    def calc_percent_error(y, predictions):
        percent_error = []
        for index in range(0, len(y)):
            percent_error.append(abs(((y[index] - predictions[index]) / (y[index])) * 100))
        return percent_error
