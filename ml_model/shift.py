from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR, LinearSVR
from sklearn.linear_model import LinearRegression, ElasticNet
from sklearn.preprocessing import PolynomialFeatures
from ml_model.model import Model
from sklearn.model_selection import GridSearchCV
from sklearn.tree import ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import Lasso
from sklearn.linear_model import LogisticRegression, SGDRegressor, Ridge


def get_shift_model():
    # param_grid = [{ 'n_estimators': [100, 200, 400, 500, 800, 1000, 1500, 2000], 'max_leaf_nodes': [400, 600, 800, 1000, 1200, 1400]}]
    # pipeline = RandomForestRegressor(n_estimators=100, max_leaf_nodes=2000)
    # forestRegresor = RandomForestRegressor()
    # pipeline = GridSearchCV(forestRegresor, param_grid, cv=5)

    # pipeline = ExtraTreeRegressor()

    pipeline = Pipeline([
        # TODO tu możemy testować wszystkie algorytmy odkomentowując itd..
        ('scaler', StandardScaler()),
        ('poly', PolynomialFeatures(degree=3)),
        #('algorithm', LinearRegression())
        ('alhorithm', SVR(kernel="rbf", C=1800, gamma='auto', epsilon=0.9))
        # ('algorithm', LinearSVR(C=8000))
       # ('alhorithm', Ridge())
        # ('alhorithm', SGDRegressor())
        # ('alhorithm', Lasso(alpha=0.43))
        #('alghorithm', RandomForestRegressor(n_estimators=100, max_leaf_nodes=1500))
        # ('alhorithm', ElasticNet())
        # ('algorithm', DecisionTreeRegressor())
    ])
    attributes_x = ['Vp', 'Squared_Vp_minus_Vk', 'Gear']  # TODO ????
    attributes_y = ['s']
    return Model(pipeline, attributes_x, attributes_y)


def get_ideal_linear_shift_model():
    pipeline = Pipeline([
        ('scaler', StandardScaler()),
        ('algorithm', LinearRegression())
    ])

    attributes_x = ['Squared_Vp_minus_Vk']
    attributes_y = ['s']
    return Model(pipeline, attributes_x, attributes_y)


def get_ideal_nonlinear_shift_model():
    pipeline = Pipeline([
        ('scaler', StandardScaler()),
        ('poly', PolynomialFeatures(degree=2)),
        ('algorithm', LinearRegression())
    ])

    attributes_x = ['Vp', 'Squared_Vp_minus_Vk']
    attributes_y = ['s']
    return Model(pipeline, attributes_x, attributes_y)
