from ml_model.model import Model
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline


def get_braking_model():
    attributes_x = ['maxValue', 'derivative']
    attributes_y = ['output']
    pipeline = Pipeline([
        ('scaler', StandardScaler()),
        ("linearSvc", LinearSVC(C=5))
    ])
    return Model(pipeline, attributes_x, attributes_y)