import functools
import os

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler

from ml_model.model import Model


def prepare_data(data):
    attributes_x = ['Vp', 'Squared_Vp_minus_Vk', 'Gear']
    attributes_y = ['s']
    # pipeline_x = Pipeline([
    #     ('convert', DataFrameSelector(attributes_x))])
    # pipeline_y = Pipeline([
    #     ('convert', DataFrameSelector(attributes_y))])
    x = data[attributes_x]
    y = data[attributes_y]
    return x, y


def run(train_data, test_data):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

    scaler = MinMaxScaler()

    x_train, y_train = prepare_data(train_data)
    x_test, y_test = prepare_data(test_data)

    [float(i) for i in y_train['s']]
    [float(i) for i in y_test['s']]

    x_test = scaler.fit_transform(x_test)
    # y_test = scaler.fit_transform(y_test)
    x_train = scaler.fit_transform(x_train)
    # y_train = scaler.fit_transform(y_train)

    # Initializers
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    n_inputs = 3
    n_neurons_1 = 1024
    n_neurons_2 = 512
    n_neurons_3 = 256
    n_neurons_4 = 128

    n_neurons_5 = 64
    n_neurons_6 = 32
    n_neurons_7 = 16
    n_neurons_8 = 8
    n_target = 1

    X = tf.placeholder(tf.float32, shape=(None, n_inputs), name="X")
    y = tf.placeholder(tf.float32, shape=None, name="y")

    training = tf.placeholder_with_default(False, shape=(), name='uczenie')
    batch_norm_layer = functools.partial(tf.layers.batch_normalization, training=training, momentum=0.9)
    # Hidden layer

    hidden1 = tf.layers.dense(X, n_neurons_1, name="ukryta1")
    bn1 = batch_norm_layer(hidden1)
    bn1_act = tf.nn.elu(bn1)

    hidden2 = tf.layers.dense(bn1_act, n_neurons_2, name="ukryta2")
    bn2 = batch_norm_layer(hidden2)
    bn2_act = tf.nn.elu(bn2)

    hidden3 = tf.layers.dense(bn2_act, n_neurons_3, name="ukryta3")
    bn3 = batch_norm_layer(hidden3)
    bn3_act = tf.nn.elu(bn3)

    hidden4 = tf.layers.dense(bn3_act, n_neurons_4, name="ukryta4")
    bn4 = batch_norm_layer(hidden4)
    bn4_act = tf.nn.elu(bn4)

    # hidden5 = tf.layers.dense(bn4_act, n_neurons_5, name="ukryta5")
    # bn5 = batch_norm_layer(hidden5)
    # bn5_act = tf.nn.elu(bn5)
    #
    # hidden6 = tf.layers.dense(bn5_act, n_neurons_6, name="ukryta6")
    # bn6 = batch_norm_layer(hidden6)
    # bn6_act = tf.nn.elu(bn6)
    #
    # hidden7 = tf.layers.dense(bn6_act, n_neurons_7, name="ukryta7")
    # bn7 = batch_norm_layer(hidden7)
    # bn7_act = tf.nn.elu(bn7)
    #
    # hidden8 = tf.layers.dense(bn7_act, n_neurons_8, name="ukryta8")
    # bn8 = batch_norm_layer(hidden8)
    # bn8_act = tf.nn.elu(bn8)

    # Output layer (must be transposed)
    out_before_bn = tf.layers.dense(bn4_act, n_target, name="wyjscie")
    out = batch_norm_layer(out_before_bn)

    mse = tf.reduce_mean(tf.squared_difference(out, y))
    opt = tf.train.AdamOptimizer().minimize(mse)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    # Setup interactive plot
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    line1, = ax1.plot(y_test)
    line2, = ax1.plot(y_test * 0.5)
    plt.show()
    with tf.Session() as net:
        init.run()
        # Number of epochs and batch size
        epochs = 10
        batch_size = 256
        extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

        for e in range(epochs):

            # Minibatch training
            for i in range(0, len(y_train) // batch_size):
                start = i * batch_size
                batch_x = x_train[start:start + batch_size]
                batch_y = y_train[start:start + batch_size]
                # Run optimizer with batch
                net.run(opt, feed_dict={X: batch_x, y: batch_y})

                # Show progress
                if np.mod(i, 5) == 0:
                    pred = net.run(out, feed_dict={X: x_test})
                    line2.set_ydata(pred)
                    plt.title('Epoch ' + str(e) + ', Batch ' + str(i))
                    plt.pause(0.01)

        pred = net.run(out, feed_dict={X: x_test})
        Model.mae(y_test, pred)
