import matplotlib.pyplot as plt
from pandas.plotting import scatter_matrix


class PlotData:
    @staticmethod
    def plot_speed_in_distance(data):
        break_release = data.loc[data['breakReleased'] == True]
        break_push = data.loc[data['breakReleased'] == False]
        plt.plot(break_release['distance'], break_release['speed'], 'b^')
        plt.plot(break_push['distance'], break_push['speed'], 'r^')
        plt.show()

    @staticmethod
    def plot_depedencies(data):
        plt.show()
        plt.plot(data['Vp'], data['RPMp'], 'r^')
        plt.show()
        plt.plot(data['s'], data['Squared_Vp_minus_Vk'], 'g^')
        plt.show()

    @staticmethod
    def plot_scatter_matrix(data):
        attributes = ['Vp', 'Vp_minus_Vk', 'Squared_Vp_minus_Vk', 'Gear', 's']
        scatter_matrix(data[attributes], figsize=(20, 20), alpha=0.3)
        plt.show()

    @staticmethod
    def plot_scatter_matrix_witouth_rpm(data):
        attributes = ['Vp', 'Vp_minus_Vk', 'Squared_Vp_minus_Vk', 's']
        scatter_matrix(data[attributes], figsize=(20, 20))
        plt.show()
