# Breaking -alghoritm
speed_difference_to_start_collecting_data = 1
amount_speed_samples_to_analize = 10
gear_difference_to_verify_if_changed = 10
minimum_derivative_for_model = 0.0
minimum_samples_for_braking_data = 20
minimum_distance_meters = 30
velocities_to_cut = [38, 35, 30, 27, 25, 22, 20, 17, 13]
count_of_data_trip_files = 20

#Czy ma wyświetlać predykcje i wyniki?
show_prediction = False


time_delay_for_save_gear = 0.5
