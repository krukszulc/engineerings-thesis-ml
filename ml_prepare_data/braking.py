import pandas as pd

import constants


def get_current_gear(speed, rpm):
    return round(rpm / speed, 2)


def check_if_gear_was_changed(current, previous):
    gear_difference = abs(previous - current)
    return gear_difference > constants.gear_difference_to_verify_if_changed


class BrakingData:
    @staticmethod
    def prepare_data_braking_charts(si_data):
        data = si_data.loc[si_data['breakReleased'] == True]
        print("Start prepare series...")
        data.reset_index(drop=True, inplace=True)
        new_data = []
        sum_of_elements = 0
        gear = 0
        a = 0
        is_collecting = False
        gear_is_saved = False
        gear_is_changed = False
        current_iteration_first_time = 0
        speed_data = data['speed']
        time_data = data['time']
        rpm_data = data["rpm"]
        for index in range(1, speed_data.size - constants.amount_speed_samples_to_analize):
            if is_collecting:
                print("Collecting data...")
                if sum_of_elements == 0:
                    current_iteration_first_time = time_data[index]

                if (gear_is_saved == False) & ((time_data[index] - current_iteration_first_time) > constants.time_delay_for_save_gear):
                    gear_is_saved = True
                    gear = get_current_gear(speed_data[index], rpm_data[index])

                if (gear_is_saved == True):
                    gear_is_changed = check_if_gear_was_changed(
                        get_current_gear(speed_data[index], rpm_data[index]),
                        gear)

                if (speed_data[index - sum_of_elements] - speed_data[index] > 0) & (a == 0):
                    a = (speed_data[index - sum_of_elements] - speed_data[index]) / (
                            time_data[index] - time_data[index - sum_of_elements])

                sum_of_elements += 1
                if BrakingData.ready_to_stop_collecting(
                        speed_data[index: (index + constants.amount_speed_samples_to_analize)]) or gear_is_changed:
                    is_collecting = False
                    if sum_of_elements > constants.minimum_samples_for_braking_data:  # is longer than 100 units / 10seconds
                        new_data.append(pd.DataFrame({
                            'distance': data['distance'][(index - sum_of_elements):index],
                            'speed': speed_data[(index - sum_of_elements):index],
                            'rpm': data['rpm'][(index - sum_of_elements):index],
                            'breakReleased': data['breakReleased'][(index - sum_of_elements):index],
                            'gear': gear,
                            'a': a,
                            'aavrg': (speed_data[index - sum_of_elements] - speed_data[index]) / (
                                        time_data[index] - time_data[index - sum_of_elements])
                        }))
            else:
                sum_of_elements = 0
                gear = 0
                a = 0
                current_iteration_first_time = 0
                gear_is_saved = False
                gear_is_changed = False
                is_collecting = BrakingData.ready_to_start_collecting(
                    speed_data.loc[index:(index + constants.amount_speed_samples_to_analize)])
        return new_data

    @staticmethod
    def ready_to_start_collecting(samples):
        samples.reset_index(drop=True, inplace=True)
        for index in range(1, samples.size):
            if samples[index - 1] - samples[index] < 0:
                return False
        return (samples[0] - samples[
            constants.amount_speed_samples_to_analize - 1]) > constants.speed_difference_to_start_collecting_data

    @staticmethod
    def ready_to_stop_collecting(samples):
        samples.reset_index(drop=True, inplace=True)
        for index in range(1, samples.size):
            if (samples[index - 1] - samples[index]) > 0:
                return False
        return (samples[0] - samples[
            constants.amount_speed_samples_to_analize - 1]) < constants.speed_difference_to_start_collecting_data

    @staticmethod
    def prepare_braking_examples(data):
        maxValue = []
        derivative = []
        output = []
        braking_by_exhaust = data.loc[data['breakReleased'] == True]
        braking_by_help = data.loc[data['breakReleased'] == False]
        braking_exhaust_series = BrakingData.prepare_data_braking_charts(braking_by_exhaust)
        braking_help_series = BrakingData.prepare_data_braking_charts(braking_by_help)
        for index in range(0, len(braking_exhaust_series)):
            maxValue.append(max(braking_exhaust_series[index]['speed']))
            shift = max(braking_exhaust_series[index]['distance']) - min(braking_by_exhaust[index]['distance'])
            diff_velocity = (max(braking_exhaust_series[index]['speed']) - min(braking_by_exhaust[index]['speed']))
            derivative.append(diff_velocity / shift)
            output.append(1)
        for index in range(0, len(braking_help_series)):
            maxValue.append(max(braking_help_series[index]['speed']))
            shift = max(braking_help_series[index]['distance']) - min(braking_by_exhaust[index]['distance'])
            diff_velocity = (max(braking_help_series[index]['speed']) - min(braking_by_exhaust[index]['speed']))
            derivative.append(diff_velocity / shift)
            output.append(0)
        dataFrame = pd.DataFrame({
            'derivative': derivative[0:],
            'maxValue': maxValue[0:],
            'output': output[0:]
        })
        dataFrame.to_csv('data/examples_for_model_braking_diff.csv')
        print("Example created...")
        return dataFrame
