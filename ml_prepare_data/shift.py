import pandas as pd

import constants
from ml_prepare_data.braking import BrakingData
from ml_prepare_data.data import Data

shift_examples_prepare_path = 'data/prepare_data/shift_examples.csv'
shift_examples_path = 'data/prepare_data/shift_examples_filtered.csv'
shift_examples_test_prepare_path = 'data/prepare_data/shift_examples_test.csv'
shift_examples_test_path = 'data/prepare_data/shift_examples_test_filtered.csv'
train_shift_path = 'data/examples_for_model/train_shift.csv'
test_shift_path = 'data/examples_for_model/test_shift.csv'


class ShiftData:
    @staticmethod
    def create_train_for_shift_model():
        for index in range(1, 16):
            data = pd.read_csv('data/raw/trasa' + str(index) + '.csv')
            data2 = Data.save_single_trip(data, index)
            ShiftData.prepare_examples_for_model(data2)
        ShiftData.filter_shift_examples()
        data = pd.read_csv(shift_examples_path)
        train = ShiftData.set_gear(data)
        train.to_csv(train_shift_path)

    @staticmethod
    def set_gear(data):
        column_name = 'Gear'
        gear_5 = (data[column_name] >= 97) & (data[column_name] <= 103)
        gear_4 = (data[column_name] >= 118) & (data[column_name] <= 128)
        gear_3 = (data[column_name] >= 170) & (data[column_name] <= 180)
        gear_2 = (data[column_name] >= 265) & (data[column_name] <= 280)
        data.loc[gear_5, column_name] = 5
        data.loc[gear_4, column_name] = 4
        data.loc[gear_3, column_name] = 3
        data.loc[gear_2, column_name] = 2
        return data


    @staticmethod
    def create_test_for_shift_model():
        for index in range(1, 5):
            data = pd.read_csv('data/raw/test' + str(index) + '.csv')
            data2 = Data.save_single_test_trip(data, index)
            ShiftData.prepare_examples_test_for_model(data2)
        ShiftData.filter_shift_test_examples()
        data = pd.read_csv(shift_examples_test_path)
        test = ShiftData.set_gear(data)
        test.to_csv(test_shift_path)

    @staticmethod
    def filter_shift_examples():
        df = pd.read_csv(shift_examples_prepare_path)
        df2 = df.drop_duplicates()
        filtered_df = ShiftData.filter_gears_for_model(df2)
        filtered_df.to_csv(shift_examples_path)

    @staticmethod
    def filter_shift_test_examples():
        df = pd.read_csv(shift_examples_test_prepare_path)
        df2 = df.drop_duplicates()
        filtered_df = ShiftData.filter_gears_for_model(df2)
        filtered_df.to_csv(shift_examples_test_path)

    @staticmethod
    def filter_gears_for_model(data):
        gear = data['Gear']
        fifth_condition = ((gear <= 103) & (gear >= 97))
        fourth_condition = ((gear <= 127) & (gear >= 119))
        third_condition = ((gear <= 180) & (gear >= 171))
        df = data.loc[fifth_condition | fourth_condition | third_condition]
        return df

    @staticmethod
    def prepare_examples_for_model(si_data):
        data_ready_to_prepare = BrakingData.prepare_data_braking_charts(si_data)
        for index in range(0, len(data_ready_to_prepare)):
            ShiftData.prepare_single_example_for_model(data_ready_to_prepare[index])
        return

    @staticmethod
    def prepare_examples_test_for_model(si_data):
        data_ready_to_prepare = BrakingData.prepare_data_braking_charts(si_data)
        for index in range(0, len(data_ready_to_prepare)):
            ShiftData.prepare_single_test_example_for_model(data_ready_to_prepare[index])
        return

    @staticmethod
    def prepare_single_test_example_for_model(data):
        vp = []
        vp_minus_vk = []
        squared_vp_minus_vk = []
        s = []
        rpmp = []
        gear = []
        a = []
        aavrg = []
        data.reset_index(drop=True, inplace=True)
        gear_data = data['gear']
        speed_data = data['speed']
        distance_data = data['distance']
        rpm_data = data['rpm']
        a_data = data['a']
        aavrg_data = data['aavrg']
        temp_s = round(max(distance_data) - min(distance_data), 2)
        if temp_s >= constants.minimum_distance_meters:
            vp.append(max(speed_data))
            gear.append(gear_data[0])
            a.append(a_data[0])
            aavrg.append(aavrg_data[0])
            vp_minus_vk.append(round((max(speed_data) - min(speed_data)), 2))
            squared_vp_minus_vk.append(
                round((max(speed_data) * max(speed_data) - min(speed_data) * min(speed_data)), 2))
            s.append(temp_s)
            rpmp.append(rpm_data[0])
        df = pd.DataFrame({
            'Vp': vp[0:],
            'Vp_minus_Vk': vp_minus_vk[0:],
            'Squared_Vp_minus_Vk': squared_vp_minus_vk[0:],
            'RPMp': rpmp[0:],
            'Gear': gear[0:],
            's': s[0:],
            'a': a[0:],
            'aavrg': aavrg[0:]
        })
        print("Save to csv, main.." + str(vp[0:]) + "  difffernce" + str(squared_vp_minus_vk[0:]))
        with open(shift_examples_test_prepare_path, 'a') as f:
            df.to_csv(f, header=False)
        print("Finish save to csv")
        return

    @staticmethod
    def prepare_single_example_for_model(data):
        vp = []
        vp_minus_vk = []
        squared_vp_minus_vk = []
        s = []
        rpmp = []
        gear = []
        a = []
        aavrg = []
        data.reset_index(drop=True, inplace=True)
        gear_data = data['gear']
        speed_data = data['speed']
        distance_data = data['distance']
        rpm_data = data['rpm']
        a_data = data['a']
        aavrg_data = data['aavrg']
        temp_s = round(max(distance_data) - min(distance_data), 2)
        if temp_s >= constants.minimum_distance_meters:
            for index, velocity in enumerate(speed_data):
                if (int(round(velocity)) in constants.velocities_to_cut) & (index > 10):
                    ShiftData.prepare_single_example_for_model_cut_down(data[index:])
                if (data[0:index].size != data.size) & (index > 10):
                    ShiftData.prepare_single_example_for_model_cut_up(data[0:index])
            vp.append(max(speed_data))
            gear.append(gear_data[0])
            a.append(a_data[0])
            aavrg.append(aavrg_data[0])
            vp_minus_vk.append(round((max(speed_data) - min(speed_data)), 2))
            squared_vp_minus_vk.append(
                round((max(speed_data) * max(speed_data) - min(speed_data) * min(speed_data)), 2))
            s.append(temp_s)
            rpmp.append(rpm_data[0])
        df = pd.DataFrame({
            'Vp': vp[0:],
            'Vp_minus_Vk': vp_minus_vk[0:],
            'Squared_Vp_minus_Vk': squared_vp_minus_vk[0:],
            'RPMp': rpmp[0:],
            'Gear': gear[0:],
            's': s[0:],
            'a': a[0:],
            'aavrg': aavrg[0:]
        })
        print("Save to csv, main.." + str(vp[0:]) + "  difffernce" + str(squared_vp_minus_vk[0:]))
        with open(shift_examples_prepare_path, 'a') as f:
            df.to_csv(f, header=False)
        return

    @staticmethod
    def prepare_single_example_for_model_cut_down(data):
        vp = []
        vp_minus_vk = []
        squared_vp_minus_vk = []
        s = []
        rpmp = []
        gear = []
        a = []
        aavrg = []
        data.reset_index(drop=True, inplace=True)
        gear_data = data['gear']
        speed_data = data['speed']
        distance_data = data['distance']
        rpm_data = data['rpm']
        a_data = data['a']
        aavrg_data = data['aavrg']
        temp_s = round(max(distance_data) - min(distance_data), 2)
        if temp_s >= constants.minimum_distance_meters:
            for index, velocity in enumerate(speed_data):
                if (data[0:index].size != data.size) & (index > 20):
                    ShiftData.prepare_single_example_for_model_cut_up(data[0:index])
            vp.append(max(speed_data))
            a.append(a_data[0])
            aavrg.append(aavrg_data[0])
            vp_minus_vk.append(round((max(speed_data) - min(speed_data)), 2))
            squared_vp_minus_vk.append(
                round((max(speed_data) * max(speed_data) - min(speed_data) * min(speed_data)), 2))
            s.append(temp_s)
            gear.append(gear_data[0])
            rpmp.append(rpm_data[0])
        print("Save to csv, cut down.." + str(vp[0:]) + "  difffernce" + str(squared_vp_minus_vk[0:]))
        df = pd.DataFrame({
            'Vp': vp[0:],
            'Vp_minus_Vk': vp_minus_vk[0:],
            'Squared_Vp_minus_Vk': squared_vp_minus_vk[0:],
            'RPMp': rpmp[0:],
            'Gear': gear[0:],
            's': s[0:],
            'a': a[0:],
            'aavrg': aavrg[0:]
        })
        with open(shift_examples_prepare_path, 'a') as f:
            df.to_csv(f, header=False)
        return

    @staticmethod
    def prepare_single_example_for_model_cut_up(data):
        vp = []
        vp_minus_vk = []
        squared_vp_minus_vk = []
        s = []
        rpmp = []
        gear = []
        a = []
        aavrg = []
        data.reset_index(drop=True, inplace=True)
        gear_data = data['gear']
        speed_data = data['speed']
        distance_data = data['distance']
        rpm_data = data['rpm']
        a_data = data['a']
        aavrg_data = data['aavrg']
        temp_s = round(max(distance_data) - min(distance_data), 2)
        if temp_s >= constants.minimum_distance_meters:
            vp.append(max(speed_data))
            vp_minus_vk.append(round((max(speed_data) - min(speed_data)), 2))
            squared_vp_minus_vk.append(
                round((max(speed_data) * max(speed_data) - min(speed_data) * min(speed_data)), 2))
            s.append(temp_s)
            a.append(a_data[0])
            aavrg.append(aavrg_data[0])
            gear.append(gear_data[0])
            rpmp.append(rpm_data[0])
        print("Save to csv, cut up.." + str(vp[0:]) + "  difffernce" + str(squared_vp_minus_vk[0:]))
        df = pd.DataFrame({
            'Vp': vp[0:],
            'Vp_minus_Vk': vp_minus_vk[0:],
            'Squared_Vp_minus_Vk': squared_vp_minus_vk[0:],
            'RPMp': rpmp[0:],
            'Gear': gear[0:],
            's': s[0:],
            'a': a[0:],
            'aavrg': aavrg[0:]
        })
        with open(shift_examples_prepare_path, 'a') as f:
            df.to_csv(f, header=False)
        return
