import pandas as pd

shift_examples_prepare_path = 'data/prepare_data/shift_examples.csv'
shift_examples_path = 'data/prepare_data/shift_examples_filtered.csv'
train_shift_path = 'data/examples_for_model/train_shift.csv'
test_shift_path = 'data/examples_for_model/test_shift.csv'


class Data:
    @staticmethod
    def save_single_trip(data, pos):
        distance_list_m = []
        speed_list_m_per_s = []
        rpm_list = []
        break_released_list = []
        time_list_s = []
        speed = data['speed']
        rpm = data['rpm']
        break_released = data['breakReleased']
        time = data['time']
        for index in range(0, data.speed.size):
            if index == data.speed.size - 1:
                df = pd.DataFrame({
                    'distance': distance_list_m[0:],
                    'speed': speed_list_m_per_s[0:],
                    'rpm': rpm_list[0:],
                    'breakReleased': break_released_list[0:],
                    'time': time_list_s[0:]
                })
                df.to_csv('data/raw_to_si/convert_to_si_trip' + str(pos) + '.csv')
                return df
            speed_m_per_s = speed[index] * 10 / 36
            time_s = time[index] / 1000
            rpm_list.append(rpm[index])
            break_released_list.append(break_released[index])
            speed_list_m_per_s.append(round(speed_m_per_s, 2))
            if index == 0:
                distance_list_m.append(0)
                time_list_s.append(0)
            else:
                time_list_s.append(round(time_s - time[0] / 1000, 2))
                temp_distance = speed_m_per_s * (time_s - time[index - 1] / 1000)
                distance_list_m.append(round(temp_distance + distance_list_m[index - 1], 2))

    @staticmethod
    def save_single_test_trip(data, pos):
        distance_list_m = []
        speed_list_m_per_s = []
        rpm_list = []
        break_released_list = []
        time_list_s = []
        speed = data['speed']
        rpm = data['rpm']
        break_released = data['breakReleased']
        time = data['time']
        for index in range(0, data.speed.size):
            if index == data.speed.size - 1:
                df = pd.DataFrame({
                    'distance': distance_list_m[0:],
                    'speed': speed_list_m_per_s[0:],
                    'rpm': rpm_list[0:],
                    'breakReleased': break_released_list[0:],
                    'time': time_list_s[0:]
                })
                df.to_csv('data/raw_to_si/convert_to_si_test_trip' + str(pos) + '.csv')
                return df
            speed_m_per_s = speed[index] * 10 / 36
            time_s = time[index] / 1000
            rpm_list.append(rpm[index])
            break_released_list.append(break_released[index])
            speed_list_m_per_s.append(round(speed_m_per_s, 2))
            if index == 0:
                distance_list_m.append(0)
                time_list_s.append(0)
            else:
                time_list_s.append(round(time_s - time[0] / 1000, 2))
                temp_distance = speed_m_per_s * (time_s - time[index - 1] / 1000)
                distance_list_m.append(round(temp_distance + distance_list_m[index - 1], 2))