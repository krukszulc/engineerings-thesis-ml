import os

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler

from ml_model.model import Model


def prepare_data(data):
    attributes_x = ['Vp', 'Squared_Vp_minus_Vk', 'Gear']
    attributes_y = ['s']
    # pipeline_x = Pipeline([
    #     ('convert', DataFrameSelector(attributes_x))])
    # pipeline_y = Pipeline([
    #     ('convert', DataFrameSelector(attributes_y))])
    x = data[attributes_x]
    y = data[attributes_y]
    return x, y


def run(train_data, test_data):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

    scaler = MinMaxScaler()

    x_train, y_train = prepare_data(train_data)
    x_test, y_test = prepare_data(test_data)

    x_test = scaler.fit_transform(x_test)
    # y_test = scaler.fit_transform(y_test)
    x_train = scaler.fit_transform(x_train)
    # y_train = scaler.fit_transform(y_train)

    # Initializers
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    n_inputs = 3
    n_neurons_1 = 1024
    n_neurons_2 = 512
    n_neurons_3 = 256
    n_neurons_4 = 128
    n_target = 1

    X = tf.placeholder(tf.float32, shape=(None, n_inputs), name="X")
    y = tf.placeholder(tf.float32, shape=None, name="y")

    # Layer 1: Variables for hidden weights and biases
    W_hidden_1 = tf.Variable(weight_initializer([n_inputs, n_neurons_1]))
    bias_hidden_1 = tf.Variable(bias_initializer([n_neurons_1]))
    # Layer 2: Variables for hidden weights and biases
    W_hidden_2 = tf.Variable(weight_initializer([n_neurons_1, n_neurons_2]))
    bias_hidden_2 = tf.Variable(bias_initializer([n_neurons_2]))
    # Layer 3: Variables for hidden weights and biases
    W_hidden_3 = tf.Variable(weight_initializer([n_neurons_2, n_neurons_3]))
    bias_hidden_3 = tf.Variable(bias_initializer([n_neurons_3]))
    # Layer 4: Variables for hidden weights and biases
    W_hidden_4 = tf.Variable(weight_initializer([n_neurons_3, n_neurons_4]))
    bias_hidden_4 = tf.Variable(bias_initializer([n_neurons_4]))

    # Output layer: Variables for output weights and biases
    W_out = tf.Variable(weight_initializer([n_neurons_4, n_target]))
    bias_out = tf.Variable(bias_initializer([n_target]))

    # Hidden layer
    hidden_1 = tf.nn.elu(tf.add(tf.matmul(X, W_hidden_1), bias_hidden_1))
    hidden_2 = tf.nn.elu(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
    hidden_3 = tf.nn.elu(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
    hidden_4 = tf.nn.elu(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))

    # Output layer (must be transposed)
    out = tf.transpose(tf.add(tf.matmul(hidden_4, W_out), bias_out))

    mse = tf.reduce_mean(tf.squared_difference(out, y))
    opt = tf.train.AdamOptimizer().minimize(mse)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    # Setup interactive plot
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    line1, = ax1.plot(y_test)
    line2, = ax1.plot(y_test * 0.5)
    plt.show()
    with tf.Session() as net:
        init.run()
        # Number of epochs and batch size
        epochs = 10
        batch_size = 256

        for e in range(epochs):

            # Minibatch training
            for i in range(0, len(y_train) // batch_size):
                start = i * batch_size
                batch_x = x_train[start:start + batch_size]
                batch_y = y_train[start:start + batch_size]
                # Run optimizer with batch
                net.run(opt, feed_dict={X: batch_x, y: batch_y})

                # Show progress
                if np.mod(i, 50) == 0:
                    pred = net.run(out, feed_dict={X: x_test})
                    line2.set_ydata(pred)
                    plt.title('Epoch ' + str(e) + ', Batch ' + str(i))
                    plt.pause(0.01)

        pred = net.run(out, feed_dict={X: x_test})
        Model.mae(y_test, pred)
    # Z = out.eval(feed_dict={X: x_test})
    # y_pred = np.argmax(Z, axis=1)
    # print(y_pred)

# with tf.Session() as sess:
#     saver.restore(sess, "./modelllo.ckpt")
#     Z = out.eval(feed_dict={X: x_test})
#     y_pred = numpy.argmax(Z, axis=1)
#     print(y_pred)
